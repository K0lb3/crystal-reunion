from UnityPy import AssetsManager
import os

PATH = os.path.dirname(os.path.realpath(__file__))
src = os.path.join(PATH,'data','raw_assets')
dst = os.path.join(PATH,'data')

def export(obj, path = None, parent_dir = None):
   data = obj.read()
   if not path:
      path = os.path.join(parent_dir, data.name)
   print(path)
   os.makedirs(os.path.dirname(path), exist_ok=True)
   if obj.type in ['Sprite','Texture2D']:
      if parent_dir:
         path += '.png'
      elif path[-4] == '.':
         path = path[:-4]+'.png'
      if obj.type == 'Texture2D':
         if os.path.exists(path):
            return        
      data.image.save(path)
   elif obj.type == "TextAsset":
      with open(path, 'wb') as f:
         f.write(data.script)
   else:
      pass
      #print(obj.type, dfp)
   

for root, dirs, files in os.walk(src, topdown=False):
   for fp in files:
      fp = os.path.realpath(os.path.join(root, fp))
      if not fp.endswith('.unity3d'):
         continue
      b = AssetsManager()
      b.load_file(fp)
      for asset in b.assets.values():
         if not asset.container:
            continue
         exported = False
         # list container items first
         for ap, obj in asset.container.items():
            if obj.type in ['Sprite','Texture2D','TextAsset']:
               export(obj, os.path.join(dst, *ap.split('/')))
               exported = True
         if not exported:
            for obj in asset.objects.values():
               if obj.type in ['Sprite','Texture2D','TextAsset']:
                  export(obj, None,fp.replace('raw_assets','assets').replace('.unity3d',''))

            