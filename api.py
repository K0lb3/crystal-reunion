import json
import os
from urllib.request import Request, urlopen

PATH = os.path.dirname(os.path.realpath(__file__))
PDATA = os.path.join(PATH,'data')
PASSETS = os.path.join(PDATA,'raw_assets')

os.makedirs(PDATA, exist_ok=True)
os.makedirs(PASSETS, exist_ok=True) 

def chuncked_download(url, cz = 8192):
    d = b''
    h = urlopen(url)
    c = h.read(cz)
    while c:
        d+=c
        c = h.read(cz)
        print(len(d))
    d+=c
    return d

class AssetsManager:
    def __init__(self, path, game_manager = None):
        self.path = path
        if game_manager:
            self.game_manager = game_manager
            self.assetlist = json.loads(chuncked_download(f"https://asset.cryuni.com/{game_manager.asset_resource['name']}?versionId={game_manager.asset_resource['version_id']}"))
            self.root_url = self.game_manager.asset_root_url
        else:
            self.assetlist = json.loads(chuncked_download("https://asset.cryuni.com/'asset_versions_StandaloneWindows_en-US_2018.json"))
            self.root_url = 'https://asset.cryuni.com/2018/StandaloneWindows/en-US'
        self.assets = {
            asset['u'].replace('~','/') : {
                'hash'  :   asset['h'],
                'size'  :   asset['cl'],
                'version':  asset['v'],
                'unique':   asset['u']
            }
            for asset in self.assetlist
        }
    
    def get_asset(self, name):
        asset = self.assets[name]
        fp = os.path.join(self.path, *name.split('/'))
        if os.path.isfile(fp) and os.path.getsize(fp) == asset['size']:
            return open(fp, 'rb').read()
        else:
            print(fp)
            url = '/'.join([self.root_url, asset['unique']])
            if self.game_manager:
                url += '?versionId='+asset['version']
            data = urlopen(url).read()
            os.makedirs(os.path.dirname(fp), exist_ok=True)
            open(fp,'wb').write(data)
            return data
        

class GameManager:
    client_version : str
    client_platform : int
    language_code : str
    server_url : str = "https://w001.cryuni.com/api"
    def __init__(self, client_version = "3.9.30", client_platform = 5, language_code = "en-US"):
        self.client_version = client_version
        self.client_platform = client_platform
        self.language_code = language_code
        
        self.server_url = self.req_json("/Startup/getServerUrl", {"client_version":client_version,"client_platform":client_platform,"language_code":language_code})['server_url']
        self.__dict__.update(
            self.req_json("/Startup/getGameSetting", {"resource_version":"2018","resource_target":"StandaloneWindows","language_code":"en-US"})
        )
        
    def request(self, path, body = {}, method = "POST"):
        req = Request(self.server_url + path, data = json.dumps(body).encode('utf8'))
        return urlopen(req).read()

    def req_json(self, path, body = {}):
        return json.loads(self.request(path, body))

    def get_master(self, name):
        return self.req_json(f"/Master/get{name.title().replace('_','')}Masters", {"option":{"version":self.master_version[name],"language_code":self.languae_code}})


if __name__ == "__main__":
    try:
        gm = GameManager()
        am = AssetsManager(PASSETS, gm)
    except:
        am = AssetsManager(PASSETS)

    for key, asset in am.assets.items():
        if 'acb' in key or 'awb' in key:
            continue
        am.get_asset(key)


"""
3.
POST http://w036.cryuni.com/api/Master/getUniversalAllianceMasters HTTP/1.1
Host: w036.cryuni.com
User-Agent: UnityPlayer/2018.4.0f1 (UnityWebRequest/1.0, libcurl/7.52.0-DEV)
Accept: */*
Accept-Encoding: identity
Connection: Keep-Alive
Cookie: sid=f0c6417mko35t9r3srg14pfc44
rhash: 28041a37-395d-4c81-a36a-dc0ace91892e
Content-Type: application/json
X-Unity-Version: 2018.4.0f1
Content-Length: 70

{"option":{"version":"2017\/09\/27 10:38:37","language_code":"en-US"}}

-> [{}]



Asset Download
GET https://asset.cryuni.com/2018/StandaloneWindows/en-US/movie~opening.usm?versionId=1ZuM_MzkKM.t86I5.wxoAUlOpMAs8fcM&time=637042661458840097 HTTP/1.1
Host: asset.cryuni.com
User-Agent: UnityPlayer/2018.4.0f1 (UnityWebRequest/1.0, libcurl/7.52.0-DEV)
Accept: */*
Accept-Encoding: identity
X-Unity-Version: 2018.4.0f1
"""