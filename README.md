# Crystal Re:Union Data-Mine

This git contains tools to download and extract assets of the game [Crystal Re:Union](http://www.cryuni.com/).

The ownership of the assets belongs to [gumi](https://gu3.co.jp).


## Scripts

### Requirements
```cmd
pip install UnityPy
```

### API
api.py contains function to download assets from the game server and updates all assets when executed directly.

### EXTRACT
extract.py extracts all images from the downloaded raw assets.
